#!/bin/bash
# Display welcome message,computer name and date
echo "++++ Backup Shell Script ++++"
echo
echo "+++ Run Time: $(date) @ $(hostname)"
echo "Masukkah Direktory Home Anda"
read folder
# Define variable
BACKUP=$folder
NOW=$(date +"%d-%m-%Y")
# Let us Start Backup
echo "+++ Dumping Home Directory to $BACKUP/$NOW......"
if 	[ -d $BACKUP/$NOW ]
then	echo "Folder Exist!"
	# Just Sleep for 3 Secs
	echo "Re-Backup ... "
	sleep 2
	rm  -r $BACKUP/$NOW/
	mkdir $BACKUP/$NOW/
	cd $BACKUP/$NOW/
	tar -czvf latest.tar.gz  $BACKUP
else 	echo "Folder Not Found!"
 	echo "Create Folder Backup ... "
	# Just Sleep for 3 Secs
	sleep 3
	mkdir $BACKUP/$NOW
	cd $BACKUP/$NOW
	tar -czvf latest.tar.gz  $BACKUP
fi
# And we are Done
echo "+++ Backup Wrote to $BACKUP/$NOW/latest.tar.gz"
